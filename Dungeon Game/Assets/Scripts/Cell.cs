﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour {

	protected Room room;
	protected int x, y;

	public void Initialize(Room room, int x, int y) {
		this.room = room;
		this.x = x;
		this.y = y;

		transform.parent = room.transform;
		transform.localPosition = new Vector3 (x, y, 0);
	}
}

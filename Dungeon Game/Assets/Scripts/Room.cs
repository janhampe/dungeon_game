﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour {

	public enum RoomDirection
	{
		Left,
		Right,
		Up,
		Down
	}

	private static RoomDirection[] directions = {
		RoomDirection.Left,
		RoomDirection.Right,
		RoomDirection.Up,
		RoomDirection.Down
	};

	public RoomDirection RandomDirection {
		get {
			List<RoomDirection> leftDirections = new List<RoomDirection> ();
			foreach (RoomDirection direction in directions) {
				if (GetConnectedRoom (direction) == null)
					leftDirections.Add (direction);
			}
			return  leftDirections [Random.Range (0, leftDirections.Count)];
		}
	}

	private Room[] connectedRooms = new Room[directions.Length];
	private DoorCell[] doors = new DoorCell[directions.Length];

	public FloorCell[] floorCellPrefabs;
	public WallCell[] wallCellPrefabs;
	public DoorCell doorCellPrefab;

	public EnemySpawner enemySpawnerPrefab;
	private int maxEnemySpawners = 3;
	private EnemySpawner[] enemySpawners = null;

	public int x, y, width, height;

	private Cell[,] cells;

	private Player player = null;

	public void Initialize(Dungeon dungeon, int x, int y, int w, int h) {
		this.x = x;
		this.y = y;
		this.height = h;
		this.width = w;

		transform.parent = dungeon.transform;
		transform.localPosition = new Vector3(x, y, 0);
	}

	public void ConnectRoom(Room other, RoomDirection direction) {
		connectedRooms [(int)direction] = other;
	}

	public static void ConnectRooms(Room r1, Room r2, Room.RoomDirection room1To2Direction) {
		r1.ConnectRoom (r2, room1To2Direction);
		r2.ConnectRoom (r1, Room.ToOppositeDirection (room1To2Direction));
	}

	public Room GetConnectedRoom(RoomDirection direction) {
		return connectedRooms [(int)direction];
	}

	public DoorCell GetDoor(RoomDirection direction) {
		return doors [(int)direction];
	}

	public void SetVisible(bool visible) {
		SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer> ();
		foreach (SpriteRenderer r in renderers) {
			r.enabled = visible;
		}
	}

	public void PlayerEnter(Player p) {
		player = p;
		p.EnterRoom (this);

		if (enemySpawners == null) {
			CreateSpawners ();
		}
	}


	public void PlayerLeave(Player p) {
		player = null;

		for (int i = 0; i < maxEnemySpawners && enemySpawners != null; i++) {
			Destroy (enemySpawners [i]);
		}
	}

	void CreateSpawners() {
		enemySpawners = new EnemySpawner[maxEnemySpawners];

		for (int i = 0; i < maxEnemySpawners; i++) {
			EnemySpawner spawner = Instantiate (enemySpawnerPrefab);
			spawner.Initialize (this, player, Random.Range(2, width-2), Random.Range(2, height-2));
			enemySpawners [i] = spawner;
		}
	}

	public bool HasDirectionsLeft() {
		foreach (Room r in connectedRooms) {
			if (r == null)
				return true; 
		}
		return false;
	}

	public void Generate() {
		cells = new Cell[width, height];

		bool hasTopDoor = connectedRooms[(int)RoomDirection.Up] != null;
		bool hasBottomDoor = connectedRooms[(int)RoomDirection.Down] != null;
		bool hasLeftDoor = connectedRooms[(int)RoomDirection.Left] != null;
		bool hasRightDoor = connectedRooms[(int)RoomDirection.Right] != null;

		int topDoorPos = 0;
		int bottomDoorPos = 0;
		int leftDoorPos = 0;
		int rightDoorPos = 0;

		if (hasTopDoor) {
			topDoorPos = Random.Range (1, width-1);
		}
		if (hasBottomDoor) {
			bottomDoorPos = Random.Range (1, width-1);
		}
		if (hasLeftDoor) {
			leftDoorPos = Random.Range (1, height-1);
		}
		if (hasRightDoor) {
			rightDoorPos = Random.Range (1, height-1);
		}

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				Cell cell;
				if (i == 0 || (i == width - 1) || (j == 0) || j == height - 1) {
					if (hasTopDoor && i == topDoorPos && j == height-1) {
						DoorCell door = Instantiate (doorCellPrefab);
						door.direction = RoomDirection.Up;
						doors [(int)door.direction] = door;
						cell = door;
					} else if (hasBottomDoor && i == bottomDoorPos && j==0) {
						DoorCell door = Instantiate (doorCellPrefab);
						door.direction = RoomDirection.Down;
						doors [(int)door.direction] = door;
						cell = door;
					} else if (hasLeftDoor && j == leftDoorPos && i == 0) {
						DoorCell door = Instantiate (doorCellPrefab);
						door.direction = RoomDirection.Left;
						doors [(int)door.direction] = door;
						cell = door;
					} else if (hasRightDoor && j == rightDoorPos && i == width-1) {
						DoorCell door = Instantiate (doorCellPrefab);
						door.direction = RoomDirection.Right;
						doors [(int)door.direction] = door;
						cell = door;
					} else {
						cell = Instantiate (wallCellPrefabs [Random.Range (0, wallCellPrefabs.Length)]);
					}
				} else {
					cell = Instantiate (floorCellPrefabs [Random.Range (0, floorCellPrefabs.Length)]);
				}
				cell.Initialize (this, i, j);
				cells[i, j] = cell;
			}
		}
	}

	public static RoomDirection ToOppositeDirection(RoomDirection d) {
		if (d == RoomDirection.Down)
			return RoomDirection.Up;
		else if (d == RoomDirection.Up)
			return RoomDirection.Down;
		else if (d == RoomDirection.Left)
			return RoomDirection.Right;
		else
			return RoomDirection.Left;
	}

}

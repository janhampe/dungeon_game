﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	private Player player;

	public void Initialize (Player p) {
		this.player = p;
	}

	void Update() {
		if (player != null) {
			Vector3 newPos = new Vector3 (player.transform.position.x, player.transform.position.y, transform.position.z);
			Vector3 moveBy = (newPos - transform.position) * 0.1f;

			transform.position = newPos;
		}
	}
}

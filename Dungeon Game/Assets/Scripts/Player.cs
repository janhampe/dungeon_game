﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player : NetworkBehaviour {
	
	public static Player localPlayer;

	[SerializeField]
	private float cooldown = 0.5f;
	private float cooldownTimer;

	public float moveSpeed = 5f;

	private Rigidbody2D rigidBody;
	private Animator animator;

	private Room currentRoom;

	public LevelManager levelManager;

	public float maxHealth = 10f, health;
	public bool dead = false;

	// Use this for initialization
	void Start () {
		// TODO: cant find level manager tag in executable??
		levelManager = (LevelManager) (GameObject.FindGameObjectWithTag ("LevelManager").GetComponent<LevelManager>());
		Debug.Assert (levelManager != null);

		rigidBody = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();

		transform.position += new Vector3 (1, 1, 0);

		if (isLocalPlayer) {
			Camera.main.GetComponent<CameraFollow> ().Initialize (this);
			levelManager.dungeon.LocalPlayer = this;

			localPlayer = this;
		}

		health = maxHealth;
	}

	public void EnterRoom(Room room) {
		currentRoom = room;
		if (isLocalPlayer) {
			levelManager.dungeon.PlayerChangedRoom (room);
		}
		else {
			if (room == localPlayer.GetRoom ()) {
				GetComponent<SpriteRenderer> ().enabled = true;
			} else {
				GetComponent<SpriteRenderer> ().enabled = false;
			}
		}
	}

	public Room GetRoom() {
		return currentRoom;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isLocalPlayer || dead)
			return;

		float dx = Input.GetAxisRaw ("Horizontal");
		float dy = Input.GetAxisRaw ("Vertical");

		rigidBody.position += new Vector2(dx, dy).normalized * moveSpeed * Time.deltaTime;

		if (dx < 0) {
			GetComponent<SpriteRenderer> ().flipX = true;
		} else if (dx > 0) {
			GetComponent<SpriteRenderer> ().flipX = false;
		}
		if (cooldownTimer > 0) {
			cooldownTimer -= Time.deltaTime;
		} else if (Input.GetMouseButtonDown (0)) {			 
			animator.SetTrigger ("Attack");
			cooldownTimer = cooldown;		
		}
	}

	public void TakeDamage(float damage) {
		if (health > 0) {
			health -= damage;

			if (health <= 0) {
				GetComponent<SpriteRenderer> ().enabled = false;
				dead = true;
			}
		}
	}
}

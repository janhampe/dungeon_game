﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dungeon : MonoBehaviour {

	// Generation Algorithm: http://www.roguebasin.com/index.php?title=Basic_BSP_Dungeon_generation

	public Room roomPrefab;

	private List<BSPNode> nodes = new List<BSPNode>();
	public List<Room> rooms = new List<Room>();

	private int maxRooms = 100, maxX = 50, maxY = 50;
	private int minRoomWidth = 5, maxRoomWidth = 20, minRoomHeight = 5, maxRoomsHeight = 20;

	private Random random = new Random ();

	// should be set by player when created
	private Player _localPlayer;
	public Player LocalPlayer {
		set {  
			if (_localPlayer == null)
				value.EnterRoom (rooms [0]);
			_localPlayer = value;
		}
		get { return _localPlayer; }
	}

	public void PlayerChangedRoom(Room room) {
		foreach (Room r in rooms) {
			r.SetVisible (false);
		}
		room.SetVisible (true);
	}

	public void Generate(int seed) {
		Random.InitState (seed);

		List<Room> tmpRooms = new List<Room>();

		int width = Random.Range (minRoomWidth, maxRoomWidth+1);
		int height = Random.Range (minRoomWidth, maxRoomWidth+1);

		Room first = Instantiate (roomPrefab);
		first.Initialize (this, 0, 0, width, height);
		tmpRooms.Add (first);
		rooms.Add (first);

		while (tmpRooms.Count > 0 && rooms.Count <= maxRooms) {
			Room tmp = tmpRooms [tmpRooms.Count - 1];
			if (!tmp.HasDirectionsLeft()) {
				tmpRooms.Remove (tmp);
			} else {
				int tries = 3;
				bool addedRoom = false;

				while (tries > 0 && !addedRoom) {
					Room.RoomDirection randomDirection = tmp.RandomDirection;
					Room newRoom = CreateRoomRandom (tmp, randomDirection);

					if (!Overlaps (newRoom, rooms) && newRoom.x < maxX && newRoom.x > -maxX && newRoom.y < maxY && newRoom.y > -maxY) {
						tmpRooms.Add (newRoom);
						rooms.Add (newRoom);
						Room.ConnectRooms (tmp, newRoom, randomDirection);
						addedRoom = true;
					} else {
						Destroy (newRoom);
					}

					tries--;
				}
				if (!addedRoom) {
					tmpRooms.Remove (tmp);
				}
			}
		}

		for (int i = 0; i < rooms.Count; i++) {
			rooms [i].Generate ();
			rooms [i].SetVisible (false);
		}
		Random.InitState (System.Environment.TickCount);
	}

	public Room CreateRoomRandom(Room old, Room.RoomDirection direction) {
		int width = Random.Range (minRoomWidth, maxRoomWidth+1);
		int height = Random.Range (minRoomWidth, maxRoomWidth+1);

		Vector2Int offset = new Vector2Int (0, 0);

		if (direction == Room.RoomDirection.Up) {
			offset.y = old.height;
			offset.x = Random.Range (-width/2+3, old.width-width/2+3);
		} else if (direction == Room.RoomDirection.Down) {
			offset.y = -height;
			offset.x = Random.Range (-width/2+3, old.width-width/2+3);
		} else if (direction == Room.RoomDirection.Right) {
			offset.y = Random.Range (-height/2+3, old.height-height/2+3);
			offset.x = old.width;
		} else if (direction == Room.RoomDirection.Left) {
			offset.y = Random.Range (-height/2+3, old.height-height/2+3);
			offset.x = -width;
		}

		Room newRoom = Instantiate (roomPrefab);
		newRoom.Initialize (this, old.x+offset.x, old.y+offset.y, width, height);
		return newRoom;
	}

	public Room CreateRoom(int x, int y, int w, int h) {
		Room room = Instantiate (roomPrefab);
		room.Initialize (this, x, y, w, h);
		return room;
	}

	private bool Overlaps(Room r1, List<Room> rooms) {
		foreach (Room r2 in rooms) {
			if (r1.x < r2.x + r2.width && r1.x + r1.width > r2.x &&
			    r1.y < r2.y + r2.height && r1.y + r1.height > r2.y) {
				return true;
			}
		}
		return false;
	}
}

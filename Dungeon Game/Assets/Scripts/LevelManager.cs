﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LevelManager : NetworkBehaviour {

	public Dungeon dungeonPrefab;
	public Dungeon dungeon;

	public GameObject enemyPrefab;

	[SyncVar]
	private int seed;

	void Start () {
		if (isServer) {
			seed = (int) (Random.value * int.MaxValue);
			//seed = 500;
		}

		GenerateLevel ();
	}

	void Update() {
		if (Input.GetKeyDown (KeyCode.Space) && isLocalPlayer) {
			CmdSpawnEnemy ();
		}
	}

	void GenerateLevel() {
		if (dungeon != null) {
			Destroy (dungeon.gameObject);
		}

		dungeon = Instantiate (dungeonPrefab);
		dungeon.Generate (seed);
	}

	[Command]
	void CmdSpawnEnemy() {
		GameObject e = Instantiate (enemyPrefab, transform.position, transform.rotation);

		NetworkServer.Spawn (e);

		Destroy (e, 5f);
	}
}

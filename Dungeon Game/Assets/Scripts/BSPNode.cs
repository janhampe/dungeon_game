﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSPNode {

	public static Dungeon dungeon;

	public BSPNode left, right, parent = null;

	public int x, y, width, height;
	private int minRoomHeight = 5, minRoomWidth = 5;
	private int maxRoomHeight = 20, maxRoomWidth = 20;

	private Room room;

	public bool splitHorizontal;

	public BSPNode(int x, int y, int w, int h) {
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;
	}

	public bool Split() {
		splitHorizontal = Random.value < 0.5f;

		// prevents too large rooms (horizontal or vertical size)
		if (width > maxRoomWidth && splitHorizontal) {
			splitHorizontal = false;
		}
		else if (height > maxRoomHeight && !splitHorizontal) {
			splitHorizontal = true;
		}

		if (!splitHorizontal && width > 2 * minRoomWidth) {
			int split = Random.Range (minRoomWidth, width-minRoomWidth);

			left = new BSPNode (x, y, split, height);
			right = new BSPNode (x+split, y, width-split, height);

		} else if (height > 2 * minRoomHeight) {
			int split = Random.Range (minRoomHeight, height-minRoomHeight);

			left = new BSPNode (x, y, width, split);
			right = new BSPNode (x, y+split, width, height-split);
		} else {
			return false;
		}

		left.parent = this;
		right.parent = this;

		return true;
	}

	public Room getRoom() {
		if (room != null) {
			return room;
		} else {
			Room leftRoom = null, rightRoom = null;
			if (left != null) {
				leftRoom = left.getRoom ();
			}
			if (right != null) {
				rightRoom = right.getRoom ();
			}

			if (leftRoom == null && rightRoom == null)
				return null;
			else if (rightRoom == null)
				return leftRoom;
			else if (leftRoom == null)
				return rightRoom;
			else if (Random.value > 0.5)
				return leftRoom;
			else
				return rightRoom;
		}
	}

	public void CreateRooms() {
		if (left != null || right != null) {
			if (left != null) {
				left.CreateRooms ();
			}
			if (right != null) {
				right.CreateRooms ();
			}
		} else {
			CreateRoom ();
		}
	}

	public void GenerateRooms() {
		if (left != null || right != null) {
			if (left != null) {
				left.GenerateRooms ();
			}
			if (right != null) {
				right.GenerateRooms ();
			}
		}

		if (room != null) {
			room.Generate ();
		}
	}

	public void ConnectRooms() {
		if (room == null) {
			left.ConnectRooms ();
			right.ConnectRooms ();
		} else {
			if (parent.left == this) {
				Room other = parent.right.farLeft ();
				if (!parent.splitHorizontal) {
					Room.ConnectRooms (room, other, Room.RoomDirection.Right);
				} else {
					Room.ConnectRooms (room, other, Room.RoomDirection.Up);
				}
			} else {
				Room other = parent.parent.right.farLeft ();
				if (!parent.splitHorizontal) {
					Room.ConnectRooms (room, other, Room.RoomDirection.Right);
				} else {
					Room.ConnectRooms (room, other, Room.RoomDirection.Down);
				}
			}
		}
	}
		

	Room farLeft() {
		if (left != null)
			return left.farLeft ();
		else
			return room;
	}

	Room farRight() {
		if (right != null)
			return right.farRight ();
		else
			return room;
	}

	void CreateRoom() {
		room = dungeon.CreateRoom (x, y, width, height);
	}

}

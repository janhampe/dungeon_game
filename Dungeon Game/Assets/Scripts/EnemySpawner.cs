﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	public Enemy enemyPrefab;

	private int maxEnemies, numEnemies = 0;

	private float spawnDelay = 0.5f, spawnTimer = 0f;

	private Room room;

	private int x, y;

	private Player player;

	public void Initialize(Room room, Player p, int x, int y) {
		this.room = room;
		this.player = player;
		this.x = x;
		this.y = y;

		transform.parent = room.transform;
		transform.localPosition = new Vector3 (x, y, 0);

		maxEnemies = Random.Range (1, 4);
	}

	// Update is called once per frame
	void Update () {
		if (numEnemies < maxEnemies) {
			spawnTimer += Time.deltaTime;

			if (spawnTimer >= spawnDelay) {
				SpawnEnemy ();
				spawnTimer = 0f;
			}
		}
	}

	void SpawnEnemy() {
		if (numEnemies >= maxEnemies)
			return;

		Enemy e = Instantiate (enemyPrefab);
		e.Initialize (room, player, x + (Random.value < 0.5 ? -1 : 1), y);

		numEnemies++;
	}
}

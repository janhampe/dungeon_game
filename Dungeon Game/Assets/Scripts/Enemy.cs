﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Enemy : NetworkBehaviour {

	public float moveSpeed = 1f;
	private bool moveToPlayer = true;

	private Rigidbody2D rigidBody;
	private Animator animator;

	private Room room;

	private float damage = 1f;

	private bool canAttack = true;
	private float attackTimer = 1.0f, attackTime = 0f;

	private Player player;

	private Player attackingPlayer;

	void Start() {
		rigidBody = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Player") {
			moveToPlayer = false;
		}
	}

	void OnCollisionExit2D(Collision2D coll) {
		if (coll.gameObject.tag == "Player") {
			moveToPlayer = true;
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag == "Player") {
			attackingPlayer = (Player) coll.gameObject.GetComponent<Player>();
		}
	}

	void OnTriggerExit2D(Collider2D coll) {
		if (coll.gameObject.tag == "Player") {
			attackingPlayer = null;
		}
	}

	public void Initialize(Room room, Player player, int x, int y) {
		this.room = room;
		this.player = player;

		transform.parent = room.transform;
		transform.localPosition = new Vector3 (x, y, 0);
	}

	void Update() {
		if (moveToPlayer && !Player.localPlayer.dead) {
			Vector3 moveDirection = (Player.localPlayer.transform.position - transform.position).normalized;
			rigidBody.MovePosition( rigidBody.position + new Vector2 (moveDirection.x, moveDirection.y) * moveSpeed * Time.deltaTime);

			if (moveDirection.x > 0) {
				GetComponent<SpriteRenderer> ().flipX = true;
			} else if (moveDirection.x < 0) {
				GetComponent<SpriteRenderer> ().flipX = false;
			}
		}

		if (!canAttack) {
			animator.SetBool ("Attack", false);
			attackTime += Time.deltaTime;

			if (attackTime >= attackTimer) {
				canAttack = true;
				attackTime = 0;
			}
		} else if (attackingPlayer != null && !attackingPlayer.dead) {
			Attack ();
		}
	}

	void Attack() {
		if (attackingPlayer == null)
			return;

		Debug.Log ("Attacking player");

		animator.SetBool ("Attack", true);

		canAttack = false;
		attackingPlayer.TakeDamage (damage);
	}
}

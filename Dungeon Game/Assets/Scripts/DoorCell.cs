﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorCell : Cell {

	public Room.RoomDirection direction;

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag == "Player") {
			Player player = coll.gameObject.GetComponent<Player>();
			Room nextRoom = room.GetConnectedRoom(direction);

			room.PlayerLeave (player);
			nextRoom.PlayerEnter (player);

			DoorCell spawnOnDoor = nextRoom.GetDoor (Room.ToOppositeDirection(direction));
			spawnOnDoor.SpawnPlayer (player);
		}
	}

	public void SpawnPlayer(Player player) {
		Vector3 offset = new Vector3 ();
		float o = 1.1f;

		float playerWidth = player.GetComponent<BoxCollider2D> ().bounds.size.x;
		float playerHeight = player.GetComponent<BoxCollider2D> ().bounds.size.y;

		if (direction == Room.RoomDirection.Up)
			offset.y -= o;
		else if (direction == Room.RoomDirection.Down)
			offset.y += o;
		else if (direction == Room.RoomDirection.Left)
			offset.x += o;
		else if (direction == Room.RoomDirection.Right)
			offset.x -= o;

		player.transform.position = transform.position + offset;
	}
}
